#ifndef IO_H
#define IO_H

#include <stdio.h>

enum file_open_status {
    OPEN_ERROR = 0,
    OPEN_OK,
    OPEN_NULL
};

enum file_close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum file_open_status read_file(const char* file_name, FILE** input);

enum file_open_status write_file(const char* file_name, FILE** output);

enum file_close_status close_file(FILE** file);

#endif
