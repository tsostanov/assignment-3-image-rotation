#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

#pragma pack(push, 1)
struct pixel
{
    uint8_t r, g, b;
};
#pragma pack(pop)

struct image
{
    uint64_t width, height;
    struct pixel *data;
};

struct pixel* create_new_data(const uint64_t width,const uint64_t height);

void delete_image(struct image* img);

#endif
