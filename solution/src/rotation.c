#include "../include/rotation.h"

#include <stddef.h>


#define EXIT_SUCCESS 0
#define EXIT_ERROR (-1)

int rotate_once(struct image* source) {
    struct pixel* new_data = create_new_data(source->width, source->height);
    if (new_data == NULL) {
        return EXIT_ERROR;
    }
    size_t counter = 0;
    for (size_t j = 0; j < source->width; j++) {
        for (size_t i = source->height; i > 0; i--) {
            new_data[counter] = source->data[(i - 1) * source->width + j];
            counter++;
        }
    }
    uint64_t last_width = source->width;
    source->width = source->height;
    source->height = last_width;

    delete_image(source);
    source->data = new_data;

    return EXIT_SUCCESS;
}



struct image* rotate(struct image* source, int angle) {
    uint8_t rotation_number= (uint8_t) (((360 - angle) / 90) % 4);
    for (uint8_t i = 0; i < rotation_number; i++) {
        if(rotate_once(source) != EXIT_SUCCESS) return NULL;
    }
    return source;
}
