#include "../include/image.h"

#include <stdlib.h>

#define STRUCT_SIZE sizeof(struct pixel)



struct pixel* create_new_data(const uint64_t width,const uint64_t height){
    struct pixel* new_data = malloc(STRUCT_SIZE * height * width);
    return new_data;
}

void delete_image(struct image* img) {
    free(img->data);
    img->data = NULL;

}

