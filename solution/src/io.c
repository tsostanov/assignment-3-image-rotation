#include "../include/io.h"
#include <stdio.h>

enum file_open_status read_file(const char* file_name, FILE** input) {
    if (file_name == NULL || input == NULL) {
        return OPEN_ERROR;
    }

    FILE* result = fopen(file_name, "rb");
    if (result == NULL) {
        return OPEN_NULL;
    }

    *input = result;
    return OPEN_OK;
}

enum file_open_status write_file(const char* file_name, FILE** output) {
    if (file_name == NULL || output == NULL) {
        return OPEN_ERROR;
    }

    FILE* result = fopen(file_name, "wb");
    if (result == NULL) {
        return OPEN_NULL;
    }

    *output = result;
    return OPEN_OK;
}

enum file_close_status close_file(FILE** file) {
    if (file == NULL || *file == NULL) {
        return CLOSE_ERROR;
    }

    int result = fclose(*file);
    if (result == 0) {
        *file = NULL;
        return CLOSE_OK;
    }

    return CLOSE_ERROR;
}
