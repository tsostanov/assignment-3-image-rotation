#include "../include/bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#define PADDING_CONST 4
#define STRUCT_SIZE sizeof(struct pixel)
#define BMP_STRUCT_SIZE sizeof(struct bmp_header)

#define BMP_BITMAP_ID 0x4D42
#define BMP_SIZE 40
#define BMP_PLANES 1
#define BIT_COUNT 24
#define BI_COMPRESSION 0
#define PELS_PER_METER_X 2834
#define PELS_PER_METER_Y 2834
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0


uint8_t static calculate_padding(uint64_t width){
    return (PADDING_CONST - (width * STRUCT_SIZE) % PADDING_CONST) % PADDING_CONST;
    }

struct bmp_header new_bmp_header(struct image const* img) {
    uint64_t size = img->height * (img->width * STRUCT_SIZE + calculate_padding(img->width));
    uint32_t width = img->width;
    uint32_t height = img->height;

    struct bmp_header header = {0};

    header.bfType = BMP_BITMAP_ID;
    header.bfileSize = BMP_STRUCT_SIZE + size;
    header.bOffBits = BMP_STRUCT_SIZE;
    header.biSize = BMP_SIZE;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = BMP_PLANES;
    header.biBitCount = BIT_COUNT;
    header.biCompression = BI_COMPRESSION;
    header.biSizeImage = size;
    header.biXPelsPerMeter = PELS_PER_METER_X;
    header.biYPelsPerMeter = PELS_PER_METER_Y;
    header.biClrUsed = BI_CLR_USED;
    header.biClrImportant = BI_CLR_IMPORTANT;
    return header;
}


enum read_status from_bmp(FILE* in, struct image* img){
    struct bmp_header header = {0};
    if (!in || !img) return READ_INVALID_HEADER;
    if (!fread(&header, BMP_STRUCT_SIZE, 1, in)) return READ_INVALID_HEADER;
    if (header.bfType != BMP_BITMAP_ID) return READ_INVALID_SIGNATURE;
    uint32_t pixels_count = header.biWidth * header.biHeight;
    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = malloc(STRUCT_SIZE * pixels_count);
    if (!img->data) return READ_MALLOC_ERROR;

    if (fseek(in, header.bOffBits, SEEK_SET) != 0) return READ_INVALID_BITS;

    const uint8_t padding = calculate_padding(header.biWidth);

    for(uint32_t i = 0; i < header.biHeight; i++){
        if (fread(img->data + i * header.biWidth, STRUCT_SIZE, header.biWidth, in) != header.biWidth)
            return READ_INVALID_BITS;
        if (fseek(in, (long) padding, SEEK_CUR) != 0) return READ_INVALID_BITS;
    }
    return READ_OK;
}



enum write_status to_bmp(FILE* out, const struct image* img){
    struct bmp_header header = new_bmp_header(img);
    if (!out || !img) return WRITE_INVALID_HEADER;
    if (!fwrite(&header, BMP_STRUCT_SIZE, 1, out)) {
        fprintf(stderr, "Error writing BMP header to the output file\n");
        return WRITE_ERROR;
    }

    const uint8_t padding = calculate_padding(img->width);
    for(size_t i = 0; i < img->height; i++){
        if (fwrite(img->data + img->width * i, STRUCT_SIZE, img->width, out) != img->width) {
            fprintf(stderr, "Error writing image data to the output file\n");
            return WRITE_ERROR;
        }
        if (fseek(out, (long) padding, SEEK_CUR) != 0) {
            fprintf(stderr, "Error writing padding to the output file\n");
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
