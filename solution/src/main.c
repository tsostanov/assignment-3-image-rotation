#include "../include/bmp.h"
#include "../include/io.h"
#include "../include/rotation.h"

#include <stdio.h>
#include <stdlib.h>

#define EXIT_SUCCESS 0
#define EXIT_ERROR (-1)

void handle_error(const char* message) {
    fprintf(stderr, "Error: %s\n", message);
    exit(EXIT_ERROR);
}

void error_message(const char* message) {
    fprintf(stderr, "Error: %s\n", message);
}

int main( int argc, char** argv ) {
    if (argc != 4){
        handle_error("Incorrect usage format. Use this form: <source-image> <transformed-image> <angle>");
    }
    int angle = (int) strtol(*(argv + 3), (char **)NULL, 10);
    if(angle % 90 != 0){
        handle_error("Incorrect angle. Supports only: 0, 90, 180, 270, -90, -180, -270");
    }
    char* input_filename = argv[1];
    char* output_filename = argv[2];

    FILE* input_file;
    enum file_open_status input_open_result = read_file(input_filename, &input_file);
    if (input_open_result == OPEN_ERROR) {
        handle_error("Specify both files");
    }
    else if (input_open_result == OPEN_NULL) {
        handle_error("Error opening the input file");
    }

    FILE* output_file;
    enum file_open_status output_open_result = write_file(output_filename, &output_file);
    if (output_open_result != OPEN_OK) {
        error_message("Error opening the input file");
        enum file_close_status input_close_result = close_file(&input_file);
        if (input_close_result != CLOSE_OK) {
            error_message("Error closing the input file");
        }
        return EXIT_ERROR;
    }

    struct image original_image = {0};
    enum read_status read_result = from_bmp(input_file, &original_image);

    if (read_result != READ_OK) {
        error_message("Error reading the image from the input file: ");
        switch (read_result) {
            case READ_INVALID_HEADER:
                error_message("Invalid BMP header");
                break;
            case READ_INVALID_SIGNATURE:
                error_message("Invalid BMP signature");
                break;
            case READ_MALLOC_ERROR:
                error_message("Memory allocation error");
                break;
            case READ_INVALID_BITS:
                error_message("Invalid image bits or read error");
                break;
            default:
                error_message("Unknown error");
                break;
        }

        enum file_close_status input_close_result = close_file(&input_file);
        if (input_close_result != CLOSE_OK) {
            error_message("Error closing the input file");
        }

        enum file_close_status output_close_result = close_file(&output_file);
        if (output_close_result != CLOSE_OK) {
            error_message("Error closing the output file");
        }

        return EXIT_ERROR;
    }



    struct image* rotated_image = rotate(&original_image, angle);
    if (rotated_image == NULL) {
        error_message("Error rotating the image");

        enum file_close_status input_close_result = close_file(&input_file);
        if (input_close_result != CLOSE_OK) {
            error_message("Error closing the input file");
        }

        enum file_close_status output_close_result = close_file(&output_file);
        if (output_close_result != CLOSE_OK) {
            error_message("Error closing the output file");
        }

        return EXIT_ERROR;
    }


    enum write_status write_result = to_bmp(output_file, rotated_image);
    if (write_result != WRITE_OK) {
        error_message("Error writing to the output file: ");

        switch (write_result) {
            case WRITE_INVALID_HEADER:
                error_message("Invalid BMP header");
                break;
            case WRITE_ERROR:
                error_message("Error writing BMP header, image data, or padding to the output file");
                break;
            default:
                error_message("Unknown error");
                break;
        }

        enum file_close_status input_close_result = close_file(&input_file);
        if (input_close_result != CLOSE_OK) {
            error_message("Error closing the input file");
        }

        enum file_close_status output_close_result = close_file(&output_file);
        if (output_close_result != CLOSE_OK) {
            error_message("Error closing the output file");
        }

        delete_image(rotated_image);

        return EXIT_ERROR;
    }

    delete_image(rotated_image);

    enum file_close_status input_close_result = close_file(&input_file);
    if (input_close_result != CLOSE_OK) {
        error_message("Error closing the input file");
    }
    enum file_close_status output_close_result = close_file(&output_file);
    if (output_close_result != CLOSE_OK) {
        error_message("Error closing the output file");
    }

    return EXIT_SUCCESS;
}
